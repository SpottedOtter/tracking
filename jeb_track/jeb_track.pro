TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

CONFIG += link_pkgconfig
PKGCONFIG += opencv

LIBS += -I /usr/include/eigen3/
SOURCES += main.cpp \
    new_eigen.cpp \
    gp.cpp

LIBS += -llapack

HEADERS += \
    f2c.h \
    clapack.h \
    new_eigen.hpp


