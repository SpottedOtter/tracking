#include <opencv2/opencv.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/core/ocl.hpp>

using namespace cv;
using namespace std;

// Convert to string
#define SSTR( x ) static_cast< std::ostringstream & >( \
( std::ostringstream() << std::dec << x ) ).str()

int main(int argc, char **argv)
{
    // List of tracker types in OpenCV 3.2
    // NOTE : GOTURN implementation is buggy and does not work.
    string trackerTypes[6] = {"BOOSTING", "MIL", "KCF", "TLD","MEDIANFLOW", "GOTURN"};
    // vector <string> trackerTypes(types, std::end(types));

    // Create a tracker
    string trackerType = trackerTypes[2];

    Ptr<Tracker> tracker;
    string cup = "/home/levy/Downloads/vot2013/cup/%8d.jpg";
    string ball = "/home/levy/Downloads/vot2014/ball/%8d.jpg";
    string jogging = "/home/levy/Downloads/vot2014/jogging/%8d.jpg";
    string singer = "/home/levy/Downloads/vot2013/singer/%8d.jpg";
    string jump = "/home/levy/Downloads/vot2013/jump/%8d.jpg";
    #if (CV_MINOR_VERSION < 3)
    {
        tracker = Tracker::create(trackerType);
    }
    #else
    {
        if (trackerType == "BOOSTING")
            tracker = TrackerBoosting::create();
        if (trackerType == "MIL")
            tracker = TrackerMIL::create();
        if (trackerType == "KCF")
            tracker = TrackerKCF::create();
        if (trackerType == "TLD")
            tracker = TrackerTLD::create();
        if (trackerType == "MEDIANFLOW")
            tracker = TrackerMedianFlow::create();
        if (trackerType == "GOTURN")
            tracker = TrackerGOTURN::create();
    }
    #endif

    // Read video
    VideoCapture video(singer);

//    tracker = TrackerMedianFlow::create();
//    trackerType = "MEDIANFLOW";

    trackerType = "TLD";
    tracker = TrackerTLD::create();

    // Exit if video is not opened
    if(!video.isOpened())
    {
        cout << "Could not read video file" << endl;
        return 1;
    }

    // Read first frame
    Mat frame;
    bool ok = video.read(frame);

    // Define initial boundibg box
    //cup = 124.67,92.308,46.73,58.572
    //jump = 328.00,189.00,53.00,50.00
    //singer = 65,65,70,270
    //ball = Point2d(200.35,159.32), Point2d(245.48,113.74)
    //jogging = Point2d(111.00,200.00), Point2d(137.00,98.00)
    Rect2d bbox(65,65,70,270);


    // Display bounding box.
    rectangle(frame, bbox, Scalar( 255, 0, 0 ), 2, 1 );
    circle(frame, Point2d(bbox.x + bbox.width/2, bbox.y + bbox.height/2), 1, Scalar(0, 0, 255), 2);
    imshow("Tracking", frame);
    cout << bbox.x + bbox.width/2 << " " << bbox.y + bbox.height/2 << '\n';
    tracker->init(frame, bbox);

    while(video.read(frame))
    {
        // Start timer
        double timer = (double)getTickCount();

        // Update the tracking result
        bool ok = tracker->update(frame, bbox);

        // Calculate Frames per second (FPS)
        float fps = getTickFrequency() / ((double)getTickCount() - timer);

        if (ok)
        {
            // Tracking success : Draw the tracked object
            rectangle(frame, bbox, Scalar( 255, 0, 0 ), 2, 1 );
        }
        else
        {
            // Tracking failure detected.
            putText(frame, "Tracking failure detected", Point(100,80), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(0,0,255),2);
        }

        // Display tracker type on frame
        putText(frame, trackerType + " Tracker", Point(100,20), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(50,170,50),2);

        // Display FPS on frame
        //putText(frame, "FPS : " + SSTR(int(fps)), Point(100,50), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(50,170,50), 2);

        // Display frame and circle.
        circle(frame, Point2d(bbox.x + bbox.width/2, bbox.y + bbox.height/2), 1, Scalar(0, 0, 255), 2);
        imshow("Tracking", frame);
        cout << bbox.x + bbox.width/2 << " " << bbox.y + bbox.height/2 << '\n';
        // Exit if ESC pressed.
        int k = waitKey(1);
        if(k == 27)
        {
            break;
        }

    }
}
